#!/usr/bin/env bash
#
# Install nginx to use as a proxy server for other services local to the machine.
# Using these instructions to configure/setup:
# https://linuxconfig.org/how-to-setup-nginx-reverse-proxy

# install nginx web server
apt install -y nginx

# disable the default virtual host
unlink /etc/nginx/sites-enabled/default

# set up proxy services for jupyterhub and rstudio
cat <<EOF > /etc/nginx/sites-available/server-proxy.conf
server {
    #server_name vagrant.rstudio.com;
    listen 80;

    rewrite ^/lab$ $scheme://$http_host/lab/ permanent; 

    location /lab/ {
      #rewrite ^/lab/(.*)$ /lab/$1 break;
      proxy_pass http://127.0.0.1:8000;
      proxy_redirect http://127.0.0.1:8000/ $scheme://$http_host/;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection $connection_upgrade;
    }

    rewrite ^/rstudio$ $scheme://$http_host/rstudio/ permanent; 
    
    location /rstudio/ {
      rewrite ^/rstudio/(.*)$ /$1 break;
      proxy_pass http://127.0.0.1:8787;
      proxy_redirect http://127.0.0.1:8787/ $scheme://$http_host/rstudio/;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection $connection_upgrade;
      proxy_read_timeout 20d;

    }

   root /var/www/html;

   location / {
      try_files $uri $uri/ = 404;
   }

}
EOF

# enable the proxy
ln -s /etc/nginx/sites-available/server-proxy.conf /etc/nginx/sites-enabled/server-proxy.conf


# change default web page

cat <<EOF > /var/www/html/index.nginx-debian.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 70em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>Available vagrant devbox servers</p>

<table style="width:100%">
  <tr>
    <th>Service</th>
    <th>Link</th>
    <th>Description</th>
  </tr>

  <tr>
    <td>JupyterHub / JupyterLab</td>
    <td><a href="http://127.0.0.1:8080/lab/">http://127.0.0.1:8080/lab/</a></td>
    <td>Jupyter hub server.  Python/Jupyther notebooks.  Scientific Python computing stack library.</td>
  </tr>

  <tr>
    <td>RStudio</td>
    <td><a href="http://127.0.0.1:8080/rstudio/">http://127.0.0.1:8080/rstudio/</a></td>
    <td>RStudio server.  R language and R Markup documents.</td>
  </tr>
</table>

</body>
</html>

EOF

# change jupyterhub server to serve on /lab/ base_url and restart the server
sed -i "s|#c.JupyterHub.base_url=''|c.JupyterHub.base_url='lab'|g" /opt/anaconda3/envs/jupyterhub/etc/jupyterhub/jupyterhub_config.py

systemctl restart jupyterhub.service
systemctl restart ngins.service
