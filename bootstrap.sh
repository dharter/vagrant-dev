#!/usr/bin/env bash

# make sure initially everything up to date
apt-get update
apt-get dist-upgrade -y
apt-get autoremove

# set timezone
timedatectl set-timezone America/Chicago

# install gnu gcc/g++ development stack, mage, git
apt-get install -y build-essential make git python3 firefox net-tools
