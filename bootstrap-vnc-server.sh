#!/usr/bin/env bash

# install tight vnc server
#apt install -y tightvncserver autocutsel

# setup a vnc server to use/share the default gnome desktop that is run
apt install -y tigervnc-scraping-server

# create appropriate xstartup file for vagrant user
umask 0077
mkdir -p /home/vagrant/.vnc
chmod go-rwx /home/vagrant/.vnc

# set default password for vnc server
/usr/bin/vncpasswd -f > /home/vagrant/.vnc/passwd << "EOF"
vagrant
vagrant
EOF

# setup an xfce 4 desktop
#cat <<"EOF" > /home/vagrant/.vnc/xstartup
##!/bin/bash
#xrdb $HOME/.Xresources
#startxvfc4 &
#EOF

# setup standard gnome desktop
# cat <<"EOF" > /home/vagrant/.vnc/xstartup
# #!/bin/sh
# [ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
# [ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
# vncconfig -iconic &
# dbus-launch --exit-with-session gnome-session &
#EOF

#chmod u+x /home/vagrant/.vnc/xstartup
chown -R vagrant:vagrant /home/vagrant/.vnc

# create systemd service using tightvnc vncserver and configure to autostart vnc service on boot
# cat <<"EOF" > /etc/systemd/system/vncserver@:1.service
# # Vncserver service file for Debian or Ubuntu with systemd
# #
# #  Install vncserver and tools
# #  e.g. apt-get install tightvncserver autocutsel gksu
# #
# # 1. Copy this file to /etc/systemd/system/vncserver@:1.service
# # 2. Edit User=
# #    e.g "User=paul"
# # 3. Edit the vncserver parameters appropriately in the ExecStart= line!
# #    e.g. the -localhost option only allows connections from localhost (or via ssh tunnels)
# # 4. Run `systemctl daemon-reload`
# # 5. Run `systemctl enable vncserver@:<display>.service`
# #

# [Unit]
# Description=Remote desktop service (VNC)
# After=syslog.target network.target

# [Service]
# Type=forking
# User=vagrant

# # Clean any existing files in /tmp/.X11-unix environment
# ExecStartPre=/bin/sh -c '/usr/bin/vncserver -kill %i > /dev/null 2>&1 || :'
# ExecStart=/usr/bin/vncserver -geometry 1920x1200 -depth 24 -dpi 120 -alwaysshared %i
# ExecStop=/usr/bin/vncserver -kill %i

# [Install]
# WantedBy=multi-user.target
# EOF

# systemctl enable vncserver
# systemctl start vncserver



# create systemd service using the tigervnc scraping server to serve the default Gnome desktop to port 5900

cat <<"EOF" > /etc/systemd/system/vncserver.service
# Vncserver service file for Debian or Ubuntu with systemd
#
#  Install vncserver and tools
#  e.g. apt-get install tightvncserver autocutsel gksu
#
# 1. Copy this file to /etc/systemd/system/vncserver@:1.service
# 2. Edit User=
#    e.g "User=paul"
# 3. Edit the vncserver parameters appropriately in the ExecStart= line!
#    e.g. the -localhost option only allows connections from localhost (or via ssh tunnels)
# 4. Run `systemctl daemon-reload`
# 5. Run `systemctl enable vncserver@:<display>.service`
#

[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target

[Service]
Type=simple
User=vagrant

ExecStart=/usr/bin/x0vncserver -passwordfile /home/vagrant/.vnc/passwd -display :0
ExecReload=/bin/kill -1 -- $MAINPID
TimeoutStopSec=5
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

systemctl enable vncserver
systemctl start vncserver
