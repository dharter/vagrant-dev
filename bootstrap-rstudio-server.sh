#!/usr/bin/env bash
#
# Assumes that R language base and RStudio installed as baseline before calling this bootstrap.
#
# Using these instructions to setup RStudio:
# https://www.edgarsdatalab.com/2017/11/24/setup-an-rstudio-server-in-ubuntu/
#
# NOTE: by default RStudio server is running on port 8787, so need to uncomment
# port forwarding for this port if want to view on host browser instead of browser in the
# guest VM

# install RStudio server
cd /vagrant # go to shared vagrant directory to download the deb file
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-1.3.959-amd64.deb
dpkg -i rstudio-server-1.3.959-amd64.deb

