#!/usr/bin/env bash
# Install a set of useful code editors / IDE tools for use on Ubuntu desktop

# make sure basic vim is installed
apt install -y vim

# Make sure emacs is installed, and set up for c++ and Python programming environments.  Also
# add in LaTeX and Markdown emacs tools.  We have a basic .emacs file to start out with a somewhat
# useful beginning configuration
apt install -y emacs 
cp /vagrant/emacs-init.el  /home/vagrant/.emacs
chown vagrant:vagrant /home/vagrant/.emacs

# install atom editor for Linux/Ubuntu, this url will always download the latest .deb file to install
#cd /vagrant 
#wget -c https://atom.io/download/deb -O atom-editor.deb
#apt install -y gconf-service gconf-service-backend gconf2-common libgconf-2-4 libpython2-stdlib libpython2.7-minimal libpython2.7-stdlib python-is-python2 python2 python2-minimal python2.7 python2.7-minimal
#dpkg -i atom-editor.deb
snap install atom --classic

# install sublime editor for Linux/Ubuntu,
snap install sublime-text --classic

# install visual code editor/ide for Linux/Ubuntu,
snap install code --classic

# install uncrustify code style checker tools and doxoygen
apt install -y uncrustify doxygen doxygen-latex graphviz

