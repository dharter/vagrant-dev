#!/usr/bin/env bash
# Add cloud9 cloud based ide server to system

# install prerequisites, cloud9 requires a python2 distribution available
apt install -y python

# for the purposes of cloud9 running, may need to remove any anaconda3
# installs from PATH, and be careful about setting PATH for the cloud9 server
#export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

# also requires node.js as a prerequisite, not sure if we need additional packages beyond base
apt install -y node-base

# run the installer from amazon
#wget -O - https://raw.githubusercontent.com/c9/install/master/install.sh | bash
cd /opt
git clone https://github.com/c9/core.git c9sdk
cd c9sdk
./scripts/install-sdk.sh

# setup vagrant user as a cloud9 dev user
cd /root
tar cvfz .c9 c9.tgz
cd /home/vagrant
tar xvfz /root/c9.tgz
chown -R vagrant:vagrant .c9

# create a cloud9 group and set permissions on the /opt/c9sdk directory so they can write
groupadd cloud9
usermod -a -G cloud9 vagrant
chgrp -R cloud9 /opt/c9sdk
chmod -R g+w /opt/c9sdk

# create a cloud9 service and start it
mkdir -p /opt/c9sdk/etc/systemd
sudo cat << EOF > /opt/c9sdk/etc/systemd/cloud9.service
[Unit]
Description=Cloud9
After=syslog.target network.target

[Service]
User=vagrant
Environment="PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:"
ExecStart=/usr/bin/node /opt/c9sdk/server.js -w /home/vagrant -a : -l 0.0.0.0 -p 8080

[Install]
WantedBy=multi-user.target

EOF

# link to OS systemd directory
sudo ln -s /opt/c9sdk/etc/systemd/cloud9.service /etc/systemd/system/cloud9.service

# start JupyterHub and enable it as a service
systemctl start cloud9.service 
systemctl enable cloud9.service
