#!/usr/bin/env bash
# Bootstrap provision a Apache guacamole html5 desktop server.  This bootstrap assumes xrdp and/or vnc servers are already
# installed and correctly serving desktops at either the standard port 3389 for xrdp or port 5900 for vnc.
# Install instructions followe:
# https://kifarunix.com/install-apache-guacamole-on-ubuntu-20-04/

# install required tools for a build from source
apt install -y gcc g++ libcairo2-dev libjpeg-turbo8-dev libpng-dev \
libtool-bin libossp-uuid-dev libavcodec-dev libavutil-dev libswscale-dev \
freerdp2-dev libpango1.0-dev libssh2-1-dev libvncserver-dev libtelnet-dev \
libssl-dev libvorbis-dev libwebp-dev libwebsockets-dev libpulse-dev

# get source tarball to extract and build
wget https://downloads.apache.org/guacamole/1.2.0/source/guacamole-server-1.2.0.tar.gz

# configure and build
tar xvfz guacamole-server-1.2.0.tar.gz
cd guacamole-server-1.2.0
./configure --with-init-dir=/etc/init.d --enable-guacenc
make
make install
ldconfig

# start and enable the guacamole server
systemctl daemon-reload
systemctl start guacd
systemctl enable guacd


# install apache tomcat9 servlet
apt install -y tomcat9 tomcat9-admin tomcat9-common tomcat9-user


# install Guacamole client
mkdir /etc/guacamole
wget https://downloads.apache.org/guacamole/1.1.0/binary/guacamole-1.1.0.war -O /etc/guacamole/guacamole.war
ln -s /etc/guacamole/guacamole.war /var/lib/tomcat9/webapps/
systemctl restart tomcat9
systemctl restart guacd

mkdir /etc/guacamole/{extensions,lib}
echo "GUACAMOLE_HOME=/etc/guacamole" >> /etc/default/tomcat9

cat <<EOF > /etc/guacamole/guacamole.properties
guacd-hostname: localhost
guacd-port:     4822
user-mapping:   /etc/guacamole/user-mapping.xml
auth-provider:  net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
EOF

ln -s /etc/guacamole /usr/share/tomcat9/.guacamole

# create desktop connections file
cat <<EOF > /etc/guacamole/user-mappings.xml
<user-mapping>
        
    <!-- Per-user authentication and config information -->

    <!-- A user using md5 to hash the password
         guacadmin user and its md5 hashed password below is used to 
             login to Guacamole Web UI-->
    <authorize 
            username="vagrant"
            password="63623900c8bbf21c706c45dcb7a2c083"
            encoding="md5">


        <!-- Allow direct connection to vnc server at port 5900 remote connection -->
        <connection name="DevBox VNC Desktop">
            <protocol>vnc</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">5900</param>
            <param name="username">vagrant</param>
	    <param name="password">vagrant</param>
            <param name="ignore-cert">true</param>
        </connection>

	
        <!-- Allow direct connection to xrdp server at port 3389 remote connection -->
        <connection name="DevBox RDP Desktop">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="port">3389</param>
            <param name="ignore-cert">true</param>
        </connection>

    </authorize>

</user-mapping>
EOF


systemctl restart tomcat9 guacd
