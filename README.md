# Ubuntu 20.04 (Focal) Desktop Dev Box #

Bootstrap / provisioning scripts for use with the
[dharter/ubuntu-20.04-dev](https://app.vagrantup.com/dharter/boxes/ubuntu-20.04-dev)
vagrant box.  The box is a fresh install of Ubuntu 20.04 Focal with the
standard Gnome desktop.

## What is this repository for? ###

The bootstrap / provisioning files in this repository install and provision
this container with useful development and work configurations.


## Usage ###

Usage of this Vagrantfile requires you to have
[Vagrant installed](https://www.vagrantup.com/docs/installation)
on your system, a to have the
[VirtualBox installed](https://www.virtualbox.org/wiki/Downloads)
to be used as the  virtualization hypervisor.

1. Clone this repository, give name for the project / development task you need,
and change into this directory to use as your current working directory.
```
$ git clone https://dharter@bitbucket.org/dharter/vagrant-dev.git project-name
$ cd project-name
```

2. Edit the Vagrantfile to enable the desired bootstrap / provisioning
installs and configurations (see below).  Alternatively, the bootstraps can be run by
hand from within the booted vagrant box.

3. Boot the vagrant box
```
$ vagrant up
```

4. Profit!


## Default User

The default username is `vagrant` with password `vagrant`.  The vagrant
user has password-less sudo admin privileges set up for the container.


## Bootstrap / Provisioning Environments ###

By default no additional development environments are configured.  The
only bootstrap provisioner run by default is the one named `bootstrap.sh`, which
does an update of the system and makes sure basic build essential tools like
`make` and `git` are installed.  You can install additional development environment
tools by uncommenting  provisioning lines like these in the Vagrantfile:

```
# config.vm.provision "shell", path: "bootstrap-python-r-analytics.sh"
```

Alternatively, your working directory will be mounted and exported to
the guest virtual machine as a directory named `/vagrant`.  So after booting
the guest machine, you can bootstrap provision one of the environments by
invoking the appropriate bootstrap script:

```
$ sudo /vagrant/bootstrap-python-r-analytics.sh
```

### Anaconda Scientific Python3 Language Development

This environment installs the most current version of the Anaconda Python3
package management system for all system users, and sets up and makes sure
the basic scientific Python development and data analytics library stack
is installed.  All necessary components to run JupyterHub/JupyterLab
notebooks are installed, though to set up the server use the separate
bootstrap provisioner below.


To bootstrap provision this environment, uncomment the following line from the
`Vagrantfile` before the first `vagrant up`, or use `vagrant provision`.

```
config.vm.provision "shell", path: "bootstrap-anaconda3-python.sh"
```

Alternatively, the bootstrap provision script can be invoked from within
the guest container to configure Anaaconda Python3 data analytics
dev environment:

```
$ sudo /vagrant/bootstrap-anaconda3-python.sh
```

### R and RStudio Language Data Analytics Development Environment

The base R system and useful data analytics libraries are installed.  RStudio is downloaded and installed, as well as needed packages
in order to use RMarkdown and create pdfs from markdown.

To bootstrap provision this environment, uncomment the following line from the
`Vagrantfile` before the first `vagrant up`, or use `vagrant provision`.

```
config.vm.provision "shell", path: "bootstrap-r-studio-language.sh"
```

Alternatively, the bootstrap provision script can be invoked from within
the guest container to configure Python and R for data analytics:

```
$ sudo /vagrant/bootstrap-r-studio-language.sh
```

### JupyterHub / JupyterLab Server

This environment performs additional installs and configurations to run
a JupyterHub/JupyterLab server.  

Several initial kernels are added for JupyterHub, including a general anaconda3
kernal with many useful scientific Python stack library packages installed by
default.  Also experimental tensorflow-gpu and pytorch-gpu kernals are
created that if working will allow passthrough use of an Nvidia GPU on the
host system from this box container.

To bootstrap provision this environment, uncomment the following line from the
`Vagrantfile` before the first `vagrant up`, or use `vagrant provision`.

```
  config.vm.provision "shell", path: "bootstrap-jupyterhub-server.sh"
```

Alternatively, the bootstrap provision script can be invoked from within
the guest container to configure Python and R for data analytics:

```
$ sudo /vagrant/bootstrap-jupyterhub-server.sh
```

Also the JupyterHub/JupyterLab server by default is running on port 8000 of the
guest box container.  If you prefer to
use your host browser to work with this containers servers, uncomment the
lines for port forwarding of these ports from the guest to the host:

```
  # JupyterHub/JupyterLab is being served at port 8000 if jupyterhub-rstudio bootstraped
  config.vm.network "forwarded_port", guest: 8000, host: 8000, host_ip: "127.0.0.1"
```

### RStudio Server

This environment performs additional installs and configurations to run
a RStudio Server on the guest, which is basically a web interface to
the RStudio IDE/dev environment..  


To bootstrap provision this environment, uncomment the following line from the
`Vagrantfile` before the first `vagrant up`, or use `vagrant provision`.

```
  config.vm.provision "shell", path: "bootstrap-rstudio-server.sh"
```

Alternatively, the bootstrap provision script can be invoked from within
the guest container to install and configure the RStudio server:

```
$ sudo /vagrant/bootstrap-rstudio-server.sh
```

Also the RStudio server runs by default on port 8787.  If you prefer to
use your host browser to work with this containers server, uncomment the
lines for port forwarding of these ports from the guest to the host:

```
  # RStudio server is being served at port 8787 if rstudio server bootstraped
  config.vm.network "forwarded_port", guest: 8787, host: 8787, host_ip: "127.0.0.1"
```

### VNC Server (Virtual Network Computing)

This bootstrap provisioner installs the Tightvnc server and configures
server to start on system boot.  **NOTE**: the vnc server currently
is having issues that need to be resolved, seems to have problems
running multiple Gnome or X windows sessions.

To bootstrap provision this environment, uncomment the following line from the
`Vagrantfile` before the first `vagrant up`, or use `vagrant provision`.

```
  config.vm.provision "shell", path: "bootstrap-vnc-server.sh"
```

Alternatively, the bootstrap provision script can be invoked from within
the guest container to install and configure the VNC server:

```
$ sudo /vagrant/bootstrap-vnc-server.sh
```

Also the VNC server desktop :1 runs on port 5901 by default.  Additional
servers if started are desktop :2 on port 5902, etc.  In order for a
VNC client to connect to the VNC served desktop, you need to uncomment
the port forwarding lines for the ports you want to use, and/or add
additional forwarded ports if using desktop or higher:

```
  # VNC Server is being served at port 5901 for desktop :1, additional desktops at 5902,5903,etc
  # config.vm.network "forwarded_port", guest: 5901, host: 5901, host_ip: "127.0.0.1"
```

### RDP Server (Remote Desktop Protocol)

This bootstrap provisioner installs the Xrdp server and configures
server to start on system boot.  **NOTE**: this server is
also currently having some configuration issues.

To bootstrap provision this environment, uncomment the following line
from the `Vagrantfile` before the first `vagrant up`, or use
`vagrant provision`.

```
  config.vm.provision "shell", path: "bootstrap-rdp-server.sh"
```

Alternatively, the bootstrap provision script can be invoked from
within the guest container to install and configure the RDP server:

```
$ sudo /vagrant/bootstrap-rdp-server.sh
```

Also the Xrdp server runs a login session manager on port 3389 by
default.  In order for RDP clients to connect to the Xrdp served
desktop sessions, you need to uncomment the port forwarding lines
for this port.

```
  # RDP Server is being served at standard port 3389 
  config.vm.network "forwarded_port", guest: 3389, host: 3389, host_ip: "127.0.0.1"
```

## Who do I talk to? ###

* derek at harter dot pro
