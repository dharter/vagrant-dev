;; make sure anaconda python is in path for python-mode and jedi
;;(setenv "PATH" (concat (getenv "PATH") ":/opt/anaconda3/bin"))

;; Basic package management, so we can load and use packages
(require 'package)
(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
	("melpa-stb" . "https://stable.melpa.org/packages/")
	("melpa" . "https://melpa.org/packages/"))
      tls-checktrust t
      tls-program '("gnutls-cli --x509cafile %t -p %p %h")
      gnutls-verify-error t)

(package-initialize)

(setq use-package-always-ensure nil)


(unless (require 'use-package nil t)
  (if (not (yes-or-no-p (concat "Refresh packages, install use-package and"
                                " other packages used by init file? ")))
      (error "you need to install use-package first")
    (package-refresh-contents)
    (package-install 'use-package)
    (require 'use-package)
    (setq use-package-always-ensure t)))


;; needed for jedi
(use-package exec-path-from-shell
  :ensure t
  :init
  )
(exec-path-from-shell-initialize)

;; set up buffer for auto completion and syntax checking
(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    ))

(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode t))

(use-package jedi
  :ensure t
  :init
  (add-hook 'python-mode-hook 'jedi:setup)
  (add-hook 'python-mode-hook 'jedi:ac-setup))

(use-package elpy
  :ensure t
  :config
  (elpy-enable))

(use-package yasnippet
  :ensure t
  :init
    (yas-global-mode 1))

  (use-package markdown-mode
    :ensure t)
;;  (use-package poly-markdown
;;    :ensure t)
  (add-to-list 'auto-mode-alist '("\\.pmd\\'" . markdown-mode))

;; c/c++ mode setup
(add-hook 'c-mode-hook 'electric-pair-mode)
(add-hook 'c++-mode-hook 'electric-pair-mode)
(defun my-c-mode-common-hook ()
  (c-set-offset 'substatement-open 0))
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)


;; some general setartup and config settings
; configure scratch startup
(setq inhibit-startup-message t)
(setq initial-scratch-message "")

; remove toolbar
(tool-bar-mode -1)

; make yes or no questions simpler
(fset 'yes-or-no-p 'y-or-n-p)

; set f5 to revert buffer back to last save
(global-set-key (kbd "<f5>") 'revert-buffer)

; display line numbers by default
(global-linum-mode)

;; a fairly nice default theme
(load-theme 'misterioso t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (markdown-mode elpy jedi htmlize ox-reveal auto-complete flycheck use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
