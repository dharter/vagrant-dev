#!/usr/bin/env bash
# Goal is to setup environment for LaTeX builds that support bibtex/biber and also using pandoc
# markdown to convert markdown -> LaTeX -> PDF

# texlive and latex packages
apt install -y texlive texlive-base texlive-bibtex-extra texlive-fonts-extra texlive-fonts-recommended texlive-latex-base texlive-latex-extra texlive-latex-recommended texlive-luatex texlive-publishers texlive-science texlive-xetex latex-make texstudio texworks elpa-markdown-mode elpa-markdown-toc biber


# make sure pandoc installed in default anaconda3 python
conda install -y pandoc markdown
conda install -y -c conda-forge pweave
